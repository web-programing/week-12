import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { Customer } from './entities/customer.entity';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer) //ฉีดเอา Customer เข้ามา
    private customerRepository: Repository<Customer>, // ตัวแปร customerRepository: เก็บข้อมูล<Customer> เอาไว้
  ) {}

  create(createCustomerDto: CreateCustomerDto) {
    // console.log(createCustomerDto);
    return this.customerRepository.save(createCustomerDto);
  }

  findAll() {
    return this.customerRepository.find({});
  }

  async findOne(id: number) {
    const customer = await this.customerRepository.findOne({
      where: { id: id },
      relations: ['orders'],
    });
    if (!customer) {
      throw new NotFoundException();
    }
    return customer;
  }

  async update(id: number, updateCustomerDto: UpdateCustomerDto) {
    const customer = await this.customerRepository.findOneBy({ id: id });
    if (!customer) {
      throw new NotFoundException();
    }
    const updatedCustomer = { ...customer, ...updateCustomerDto };
    return this.customerRepository.save(updatedCustomer);
  }

  async remove(id: number) {
    const customer = await this.customerRepository.findOneBy({ id: id });
    if (!customer) {
      throw new NotFoundException();
    }
    return this.customerRepository.softRemove(customer);
  }
}
