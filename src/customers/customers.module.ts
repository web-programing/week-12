import { Module } from '@nestjs/common';
import { CustomersService } from './customers.service';
import { CustomersController } from './customers.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Customer } from './entities/customer.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Customer])], //forFeature ใช้อ้างอิงตัวที่จะข้ามมาใช้ในนี้ แต่ถ้าอ้างอิงถึง entityเฉยๆก็ไม่ต้องใส่อะไรข้างในก็ได้
  controllers: [CustomersController],
  providers: [CustomersService],
})
export class CustomersModule {}
