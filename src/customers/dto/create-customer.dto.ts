import { IsInt, IsNotEmpty, Length, Max, Min } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(4, 999)
  name: string;

  @IsInt()
  @Min(0)
  @Max(200)
  @IsNotEmpty()
  age: number;

  @IsNotEmpty()
  @Length(10, 10)
  tel: string;

  @IsNotEmpty()
  @Length(1, 1)
  gender: string;
}
