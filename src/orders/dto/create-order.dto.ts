import { IsInt, IsNotEmpty, Length, Max, Min } from 'class-validator';

class CreateOrderItemDto {
  @IsInt()
  @Min(0)
  @IsNotEmpty()
  productId: number;

  @IsInt()
  @Min(0)
  @IsNotEmpty()
  amount: number;
}

export class CreateOrderDto {
  @IsInt()
  @Min(0)
  @IsNotEmpty()
  customerId: number;

  orderItems: CreateOrderItemDto[];
}
