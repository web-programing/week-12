import { IsInt, IsNotEmpty, Length, Max, Min } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(4, 999)
  name: string;

  @IsInt()
  @Min(0)
  @Max(9999)
  @IsNotEmpty()
  price: number;
}
